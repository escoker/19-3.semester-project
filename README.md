![Build Status](https://gitlab.com/EAL-ITT/19a-itt1-embedded-systems/badges/master/pipeline.svg)


# 19-3.semester-project

weekly plans, resources and other relevant stuff for the 3. semester projects work in IT technology 3. semester autumn of 2018 class.

public website for students:

*  [gitlab pages](https://eal-itt.gitlab.io/19-3.semester-project/)