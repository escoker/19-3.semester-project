---
Week: 43
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 43 ITT1 Embedded Systems

## Goals of the week(s)
Students will be introduced to computer architecture. Basic concept such as CPU, Memory, Registers, Operating Systems, and Kernel will be presented.

### Practical goals

1. Students will be asked to create a simple software that will read CPU load.
2. Students will be asked to create a simple software that will read Memory.
3. Students will be asked to check disk space.

### Learning goals

1. BASH scripting.
2. Using BASH identify system parameters and peripheral.


## Deliverable

1. Students will deliver the output of the du, df free and w commands.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
N/A
