---
title: '3.sem project'
subtitle: 'Weekly plans'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>','Ilas Esmati \<iles@ucl.dk\>']
main_author: 'Ilas Esmati'
date: \today
email: 'iles@ucl.dk'
left-header: \today
right-header: 'ITT1 embedded systems, weekly plans'
---


Introduction
====================

This document is a collection of weekly plans. It is based on the wekly plans in the administrative repository, and is updated automatically on change.

The sections describe the goals and programme for each week of the embedded systems classes.
