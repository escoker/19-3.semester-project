---
Week: 44
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 44 ITT1 Embedded Systems

## Goals of the week(s)
Overclocking Raspberry PI. A prime calculation algorithm will be implemented. the RPI will be pushed to the limit.

### Practical goals

1. Students will be asked to write a software that will calculate prime numbers.
2. The RPI will have to be over clocked and then the test is executed again.
3. Students will have to do the prime number calculation while read analog values from a voltage source.

### Learning goals

1. Understand of hardware limitations.
2. Understanding of voltages damaging effects on a circuit.


## Deliverable

1. Students will have to document analog input from voltage sources. During prime number calculation, with and without overclocked CPU.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
N/A
