---
Week: 36
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 36 ITT1 Embedded Systems

## Goals of the week(s)
Introduction to Raspberry PI, general and basic knowledge about micro programming environment. Simple LED theory, simple binary 0/1 logic.

### Practical goals
1. Set up Raspberry PI.
2. Set up your GitLab.
3. Set up your raspberry PI so that you can remotely access it.
4. Create a simple LED control program for three LEDs.
5. Send ON/OFF command and measure with a voltmeter that the voltage changes between 0 and 3 volts.

### Learning goals
1. Learn what LED is. 
2. Learn simple binary 0/1 logic.

## Deliverable

1. Hand in Gitlab, voltmeter values and document with a picture.
2. Hand in Gitlab, the python code.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
N/A
