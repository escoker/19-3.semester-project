---
Week: 38
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 38 ITT1 Embedded Systems

## Goals of the week(s)
Students will be introduced to signals, analog and digital, focus on Pulse-width modulation (PWM) as a digital control signal.

### Practical goals
1. Create a simple ON/OFF (Blink) LED control software controlled by PWM.
2. Update the existing circuit to veroboard. Add buttons and connect them as inputs.
3. Create a menu, blink or answer questions.

### Learning goals
1. Various signal types.
2. Pulse width modulation.
3. Controlling with signal.

## Deliverable

1. Hand in Gitlab, the python code.
2. Hand in Gitlab, circuit design.
3. Hand in Gitlab, pictures of your set up and a working solution.


## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
N/A
