---
Week: 37
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 37 ITT1 Embedded Systems

## Goals of the week(s)
Simple system creation, based about LEDs and overview of GPIOs and two state concept. Input and output concept with buttons will be shown.

### Practical goals
1. Create a simple ON/OFF LED control software.
2. Create a simple LEDs circuit on breadboard.
3. Command remotely RPI to light up LEDs.

### Learning goals
1. pull up/pull down resistors on buttons
2. LED circuit.

## Deliverable

1. Hand in Gitlab, the python code.
2. Hand in Gitlab, circuit design.
3. Hand in Gitlab, pictures of your set up and a working solution.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
N/A
