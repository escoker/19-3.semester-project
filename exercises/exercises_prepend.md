---
title: 'ITT1 Embedded Systems'
subtitle: 'Exercises'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>']
main_author: 'Ruslan Trifonov'
date: \today
email: 'rutr@ucl.dk'
left-header: \today
right-header: 'ITT1 embedded systems, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans. 

References

* [Weekly plans](https://eal-itt.gitlab.io/19a-itt1-embedded-systems/19A_ITT1_weekly_plans.pdf)



