import gitlab
# import sys


class Deliverables():
    def __init__(self, gl_obj, project_name):
        self.project_name = project_name.strip()
        self.gl_obj = gl_obj

        try:
            self.gl_project = gl_obj.projects.get(self.project_name)
            self.hasError = False
        except gitlab.exceptions.GitlabError:
            self.gl_project = None
            self.hasError = True

    def fetch_issues(self, label=None, state='opened'):
        if not self.gl_project:
            return []

        if label:
            return self.gl_project.issues.list(labels=[label], state=state)
        else:
            return self.gl_project.issues.list(state=state)

    def __repr__(self):
        return "<Deliverables: {} ({}) >".format(self.project_name, "Load error" if self.hasError else "No Error")

    @property
    def name(self):
        return self.project_name

    @property
    def web_url(self):
        try:
            return str(self.gl_project.web_url)
        except AttributeError:
            return None

    @property
    def open_issues_count(self):
        return self.gl_project.open_issues_count

    @property
    def name_with_namespace(self):
        try:
            return self.gl_project.name_with_namespace
        except AttributeError:
            return None
