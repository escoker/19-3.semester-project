from unittest import TestCase
import gitlab
import sys

from deliverables import Deliverables

token_filename="token.txt"
test_project_name_good="dofix/itt2_project_dofix"
test_name_w_name_space_good = "Dofix / ITT2_project_dofix"
test_project_name_bad="xyz_æøå/xyz_æøå"

def get_token( filename='token.txt', depth=0 ):
    max_depth=2
    try:
        with open( '../'*depth+token_filename) as f:
            token = f.readline().strip()
            sys.stderr.write("reading token from file {}\n".format('../'*depth+token_filename))
            return token
    except FileNotFoundError as e:
        if depth > max_depth:
            raise e
        return get_token( filename, depth+1)

class TestDeliverables(TestCase):
    def setUp(self):
        token = get_token(token_filename )

        self.gl = gitlab.Gitlab('https://gitlab.com', private_token=token)
        self.gl.auth()


    def test_project_name_good(self):
        d = Deliverables( self.gl, test_project_name_good )
        name = d.project_name
        self.assertEqual( name, test_project_name_good)
        self.assertIs( type(name), type( u'' ))

    def test_project_name_bad(self):
        d = Deliverables( self.gl, test_project_name_bad )
        name = d.project_name
        self.assertEqual( name, test_project_name_bad)
        self.assertIs( type(name), type( u'' ))

    def test_web_url_good(self):
        d = Deliverables( self.gl, test_project_name_good )
        self.assertIs( type(d.web_url), type( u'' ))

    def test_web_url_bad(self):
        d = Deliverables( self.gl, test_project_name_bad )
        self.assertEqual( d.web_url, None)

    def test_has_errors_good(self):
        d = Deliverables( self.gl, test_project_name_good )
        self.assertFalse( d.hasError )

    def test_has_errors_bad(self):
        d = Deliverables( self.gl, test_project_name_bad )
        self.assertTrue( d.hasError )

    def test_project_name_w_namespace_good(self):
        d = Deliverables( self.gl, test_project_name_good )
        name = d.name_with_namespace
        self.assertEqual( name, test_name_w_name_space_good)
        self.assertIs( type(name), type( u'' ))

    def test_project_name_w_namespace_bad(self):
        d = Deliverables( self.gl, test_project_name_bad )
        self.assertEqual( d.name_with_namespace, None)
